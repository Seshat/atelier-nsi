---
author: PF Villard
title: Réseaux de neurones profonds
progress: true
slideNumber: true
navigationMode: 'linear'
---


## Présentation<!-- omit in toc -->

- Victoire de Deep Blue sur Garry Kasparov &rarr; **1996**
- Repérer un chat sur une photo &rarr; **?**
- &rarr; Image traitée par **modules sensoriels** spécialisés du cerveau
- &rarr;  largement prétraitée + subjectif
  
![](https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/Cat_poster_1.jpg/1920px-Cat_poster_1.jpg){height=200px}


## CNN: Convolutional Neural Network <!-- omit in toc -->
- Apparu début 1980
- Croissance grâce à l'**évolution de la technologie**
- Domaine :
    - perception visuelle
    - reconnaissance vocale 
    - traitement automatique du langage naturel

![](https://i1.wp.com/imalogic.com/blog/wp-content/uploads/2016/04/PRINCIPE.gif){height=200px}


## Expérience de Hubel & Wiesel (1960)<!-- omit in toc -->
 - <a href="https://en.wikipedia.org/wiki/David_H._Hubel">David Hubel</a> et<a href="https://en.wikipedia.org/wiki/Torsten_Wiesel">Torsten Wiesel</a> sur la structure du cortex visuel
 - certains neurones réagissent uniquement aux images de **lignes horizontales**
 - d’autres réagissent uniquement aux lignes ayant d’**autres orientations**
 - certains neurones ont des champs récepteurs plus larges et ils réagissent à des **motifs plus complexes**
    - &rarr; neurones de plus **haut niveau** se fondent sur sorties neurones voisins de plus **bas niveau**
  
 ![](https://ml4a.github.io/images/figures/hubel-wiesel.jpg){height=200px}
 

## Couche de convolution

- neurones **pas connectés à chaque pixel** de l’image d’entrée
- neurones d'une couche de convolution : connectés aux neurones à l’intérieur d’un **petit rectangle** de la première couche
    - &rarr; se focalise sur des caractéristiques de **bas niveau**
    - &rarr; assemble en caractéristiques de plus **haut niveau**

![](https://upload.wikimedia.org/wikipedia/commons/thumb/6/63/Typical_cnn.png/614px-Typical_cnn.png){height=150px}
![](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTAbIISsSKiGEBYik-le-er4RixPpChoseFueBkJXc5gmTiph34&s){height=150px}


## Démo<!-- omit in toc -->
<iframe src="https://ml4a.github.io/demos/convolution/" width="900" height="780" style="border: none;"></iframe>


<iframe src="
https://ml4a.github.io/demos/convolution_all/" width="1200" height="780" style="border: none;"></iframe>

## Couches de pooling <!-- omit in toc -->

- objectif = sous-échantillonner (rétrécir) 
    - **&rarr;** charge de calcul &searr;
    - **&rarr;** utilisation de la mémoire &searr;
    - **&rarr;** nombre de paramètres &searr; (risque de surajustement &searr;)
- Paramètres :
    - taille
    - pas
    - type de remplissage
    - **&rarr;** pas de poids (se contente d’agréger)

![](https://upload.wikimedia.org/wikipedia/commons/e/e9/Max_pooling.png){height=200px}
![](../img/pooling.png){height=200px}


## Empiler plusieurs caractéristiques<!-- omit in toc -->

- chaque couche de convolution = plusieurs **cartes de caractéristiques** de taille égale
- une carte de caractéristiques = **mêmes paramètres** (poids et terme constant)
- **plusieurs filtres** simultanément appliqués par couche
- **&rarr;** détecter plusieurs caractéristiques
- **+** images d’entrée = multiples sous-couches &rarr; **canaux de couleur**

![](https://wizardforcel.gitbooks.io/scikit-and-tensorflow-workbooks-bjpcjp/pics/stacking-feature-maps.png){height=250px}



## Pipeline complet

- Pixels de l'image brute à gauche (ex : 256x256x3)
- Ensuite : **filtres de convolutions**
  -  Petits filtres qui **glissent** sur l'image
  -  Excités par différentes **caractéristiques** de l'image : 
     -  Petit bord horizontal
     -  Régions de couleur rouge
     -  etc. 
  -  Si 10 filtres : image originale (256,256,3) &rarr; (256,256,10)
  <!-- dans laquelle nous avons supprimé les informations de l'image originale et ne conservons que les 10 réponses de nos filtres à chaque position de l'image. C'est comme si les trois canaux de couleur (rouge, vert, bleu) étaient maintenant remplacés par 10 canaux de réponse de filtre (je les montre le long de la première colonne immédiatement à droite de l'image dans le gif ci-dessus). -->

![](http://karpathy.github.io/assets/selfie/gif2.gif)
  



## Augmentation de données

- &nearr; artificiellement jeu d'entraînement
- Nécessaire si **base de données trop petite**
- Permet de **régulariser** le modèle (&searr; surajustement)
- = Décaler, pivoter, redimensionner, changer le contraste, générer des images en miroir, etc.
- **Générer à la volée** à l'entraînement &rarr; RAM &searr;

![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/92/An_Example_of_Data_Augmentation_via_Augmentor.png/220px-An_Example_of_Data_Augmentation_via_Augmentor.png)

## Exemple

![](../img/augm.png)



<!--## Synthèse de données   omit in toc 

- Même idée que précédemment
- Utilisation de génération par images de synthèses

<iframe src="https://homepages.loria.fr/PFVillard/panneaux-routiers/PT4.html"  height="500" width="1000"></iframe>

___

<iframe src="https://homepages.loria.fr/PFVillard/grille/index.html"  height="500" width="1000"></iframe>-->




## Architectures classiques <!-- omit in toc -->

- Empilement de :
    -  une ou deux couches de **convolution**
    -  une couche de **pooling**
    -  une ou deux couches de **convolution**
    -  une autre couche de **pooling**
    -  ainsi de suite**...**
- **&rarr;** les cartes de caractéristiques rétrécissent au fur et à mesure que l’on traverse le réseau
- **&rarr;** nombre de cartes de caractéristiques  &nearr; 
- sommet de la pile : quelques couches **fully connected** 
- couche finale = prédiction **&rarr;**  couche softmax
  
![](https://upload.wikimedia.org/wikipedia/commons/thumb/6/63/Typical_cnn.png/395px-Typical_cnn.png){height=150px}


## VGG19

- **19 couches** :
  - 16 convolution layers, 3 Fully connected layer, 5 MaxPool layers et 1 SoftMax layer
  
![](../img/VGG19.png)


## Conclusion <!-- omit in toc -->

- Les évolutions sont rapides
- Nouvelles sortes d’architectures proposées chaque année
- Les CNN sont :
    - de + en + profonds
    - de + en + légers
        - &larr; de - en - de paramètres
- [Lien vers un super article](https://seshat.gitlabpages.inria.fr/deeplearning/pdf/TSP_FDMP_21726.pdf)


## Suite

[Application à la Reconnaissance automatique de personnages](../asterix.html)