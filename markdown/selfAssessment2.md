---
author: PF Villard
title: Deep Learning
subtitle: Auto evaluation 2
progress: true
slideNumber: true
navigationMode: 'linear'
---
<style>
ul.noBullet{
  list-style: none;
}

input[type="checkbox"]{
  display: none;
  border: none !important;
  box-shadow: none !important;
}

input[type="checkbox"] + label span {   
  background: url(https://gitlab.inria.fr/Seshat/unity/-/raw/master/common/reveal/css/theme/uncheck.png);
  width: 49px;
  height: 49px;
  display: inline-block;
  vertical-align: middle;
}
.opaque {
  opacity: 0;
}

input[type="checkbox"]:checked + label span {
  background: url(https://gitlab.inria.fr/Seshat/unity/-/raw/master/common/reveal/css/theme/check_2.png);
  width: 49px;
  height: 49px;
  vertical-align: middle;
}
</style>
# Réseau de neurones <!-- omit in toc --> 

## Exemple 1

- Quelle **architecture** est représenté par ce schéma ?
- Combien vaut **n**, la taille des neurones d'entrée ?
- Combien y a-t-il de **classes** ?
 
<iframe src="
https://ml4a.github.io/demos/forward_pass_mnist/" width="1200" height="780" style="border: none;"></iframe>



## Définir cette architecture
  
<ul class="noBullet">
<li><input type="checkbox" id="option8"/>
<label for="option8"> <span></span>```Perceptron percep = Perceptron(734)```  </label></li>
<li><input type="checkbox" id="option9"/>
<label for="option9"> <span></span> ```Perceptron percep = Perceptron(max_iter=1000, tol=1e-5, random_state=100)```  </label></li>
<li><input type="checkbox" id="option10"/>
<label for="option10"> <span></span> ```model = tf.keras.models.Sequential([
  tf.keras.layers.Dense(734, activation='relu'),
  tf.keras.layers.Dense(1, activation='sigmoid'),
])``` </label></li>
<li><input type="checkbox" id="option11"/>
<label for="option11"> <span></span> ```model = tf.keras.models.Sequential([
  tf.keras.layers.Dense(734, activation='relu'),
  tf.keras.layers.Dense(300, activation='relu'),
  tf.keras.layers.Dense(300, activation='relu'),
  tf.keras.layers.Dense(1, activation='sigmoid'),
])``` </label></li>
<li><input type="checkbox" id="option12"/>
<label for="option12"> <span></span> ```percep = LogisticRegression(solver="liblinear", random_state=100)``` </label></li>
</ul>

## Entraîner le modèle
  
 
<ul class="noBullet">
<li><input type="checkbox" id="option5"/>
<label for="option5"> <span></span>```percep.fit(X, y)```  </label></li>
<li><input type="checkbox" id="option6"/>
<label for="option6"> <span></span> ```percep.train(X, y)```  </label></li>
<li><input type="checkbox" id="option7"/>
<label for="option7"> <span></span> ```percep.train()``` </label></li>
<li><input type="checkbox" id="option8"/>
<label for="option8"> <span></span> ```percep.fit()``` </label></li>
</ul>

## Tester le modèle

<ul class="noBullet">
<li><input type="checkbox" id="option20"/>
<label for="option20"> <span></span>```percep.predict(X_test)```  </label></li>
<li><input type="checkbox" id="option21"/>
<label for="option21"> <span></span> ```percep.predict()```  </label></li>
<li><input type="checkbox" id="option22"/>
<label for="option22"> <span></span> ```percep.test(X_test)``` </label></li>
<li><input type="checkbox" id="option23"/>
<label for="option23"> <span></span> ```percep.test()``` </label></li>
</ul>


## Exemple 2 (suite)

- Quelle **architecture** est représenté par ce schéma ?
- Combien vaut **n**, la taille des neurones d'entrée ?
- Combien de couches cachées ?
- Combien y a-t-il de **classes** ?
- Citer des hyper-paramètres
  
![](https://upload.wikimedia.org/wikipedia/commons/a/a9/Perceptron_4layers.png)


## Etapes classiques

- Quelles sont les étapes classiques relatives à l'utilisation d'une architecture pour l'apprentissage automatique ?
- Quelles peuvent être les inconnus à chaque étape ?

![](https://upload.wikimedia.org/wikipedia/commons/a/a9/Perceptron_4layers.png)  

## Définir cette architecture
   
<ul class="noBullet">
<li><input type="checkbox" id="option1"/>
<label for="option1"> <span></span> ```Perceptron percep = Perceptron(max_iter=1000, tol=1e-5, random_state=100)```  </label></li>
<li><input type="checkbox" id="option2"/>
<label for="option2"> <span></span> ```model = tf.keras.models.Sequential([
  tf.keras.layers.Dense(3, activation='relu'),
  tf.keras.layers.Dense(1, activation='sigmoid'),
])``` </label></li>
<li><input type="checkbox" id="option3"/>
<label for="option3"> <span></span> ```model = tf.keras.models.Sequential([
  tf.keras.layers.Dense(3, activation='relu'),
  tf.keras.layers.Dense(4, activation='relu'),
  tf.keras.layers.Dense(3, activation='relu'),
  tf.keras.layers.Dense(4, activation='sigmoid'),
])``` </label></li>
<li><input type="checkbox" id="option4"/>
<label for="option4"> <span></span> ```model = tf.keras.models.Sequential([
  tf.keras.layers.Dense(4, activation='relu'),
  tf.keras.layers.Dense(3, activation='relu'),
  tf.keras.layers.Dense(1, activation='sigmoid'),
])``` </label></li>
</ul>

## Courbes d'apprentissage (1)

- Problème ?
- Solution ?
  
![](https://machinelearningmastery.com/wp-content/uploads/2019/02/Example-of-Training-Learning-Curve-Showing-An-Underfit-Model-That-Does-Not-Have-Sufficient-Capacity.png){height=400px}

## Courbes d'apprentissage (2)

- Problème ?
- Solution ?
  
![](https://machinelearningmastery.com/wp-content/uploads/2018/12/Example-of-Training-Learning-Curve-Showing-An-Underfit-Model-That-Requires-Further-Training.png){height=400px}

## Courbes d'apprentissage (3)

- Problème ?
- Solution ?
  
![](https://machinelearningmastery.com/wp-content/uploads/2018/12/Example-of-Train-and-Validation-Learning-Curves-Showing-An-Overfit-Model.png){height=400px}

## Courbes d'apprentissage  (4)

- Problème ?
- Solution ?
  
![](https://machinelearningmastery.com/wp-content/uploads/2018/12/Example-of-Train-and-Validation-Learning-Curves-Showing-A-Good-Fit.png){height=400px}

## Courbes d'apprentissage (5)

- Problème ?
- Solution ?
  
![](https://machinelearningmastery.com/wp-content/uploads/2018/12/Example-of-Train-and-Validation-Learning-Curves-Showing-a-Training-Dataset-the-May-be-too-Small-Relative-to-the-Validation-Dataset.png){height=400px}


## Courbes d'apprentissage (6)

- Problème ?
- Solution ?
  
![](https://machinelearningmastery.com/wp-content/uploads/2018/12/Example-of-Train-and-Validation-Learning-Curves-Showing-a-Validation-Dataset-the-May-be-too-Small-Relative-to-the-Training-Dataset.png){height=400px}


## Courbes d'apprentissage (7)

- Problème ?
- Solution ?
  
![](https://machinelearningmastery.com/wp-content/uploads/2018/12/Example-of-Train-and-Validation-Learning-Curves-Showing-a-Training-Dataset-the-May-be-too-Small-Relative-to-the-Validation-Dataset.png){height=400px}



## Courbes d'apprentissage (8)

- Problème ?
- Solution ?
  
![](https://machinelearningmastery.com/wp-content/uploads/2018/12/Example-of-Train-and-Validation-Learning-Curves-Showing-a-Validation-Dataset-that-is-Easier-to-Predict-than-the-Training-Dataset.png){height=400px}

## Exemple 3

- Application pour **montre connectée**
- Objectif : reconnaitre un **sport pratiqué**
- Idée : dès que **>100bbm** mesuré, prédire le sport en fonction des mesures :
    - 50 mesures données par l'application
    - Choisir parmi 4 sports
- Fonction enregistrement : l'utilisateur définit le sport qu'il est en train de faire
    - 480 activités ainsi enregistrées

![](https://upload.wikimedia.org/wikipedia/commons/thumb/e/ec/Apple_Watch_Series_3_Sensors.jpg/220px-Apple_Watch_Series_3_Sensors.jpg)

## Exemple 3 (suite)

- Que valent $n$ et $m$ ?
- Où sont $X$, $\Theta$ et $y$
- Quel est l'**inconnu** pour construire le modèle ?
- Quelle **architecture** allons nous utiliser ?
- Combien de **classes** ?

## Exemple 4

- IA pour détecter une grossesse
- Pour une enseigne de supermarché
- Un ensemble d'information est collecté grâce à la carte de fidélité enregistrant les achats (500)
- Chez 300 clients, l'enseigne a su qu'il y avait eu une grossesse ou pas

![](https://gitlab.inria.fr/Seshat/internetprivacy/-/raw/master/img1/target.jpg)


## Exemple 4 (suite)

- Que valent $n$ et $m$ ?
- Où sont $X$, $\Theta$ et $y$
- Quel est l'**inconnu** pour construire le modèle ?
- Quelle **architecture** allons nous utiliser ?
- Combien de **classes** ?

## Exemple 5 

- Utilisation d'IA pour *améliorer* la santé de nos villes
- Article [ici](https://www.archdaily.com/978184/how-can-data-and-ai-improve-the-health-of-our-cities?utm_medium=email&utm_source=ArchDaily%20List&kth=1,352,573)
- **Défis sanitaires** : maladies cardiovasculaires, autres maladies chroniques, pressions en raison de l'urbanisation rapide et de la pandémie de COVID-19, ...
- **&rarr;** Mettre en œuvre des **politiques**, des **partenariats** et des **stratégies** d'investissement pour favoriser la transformation numérique
- **&#9888;** Biais algorithmiques, confidentialité des données, manque d'infrastructures et de capacités techniques

![](https://images.adsttc.com/media/images/6228/ece6/3e4b/31bf/ae00/0001/slideshow/sp-07.jpg?1646849249){width=30%}


## Exemple 5 (suite)

- Que valent $n$ et $m$ ?
- Où sont $X$, $\Theta$ et $y$ ?
- Quel est l'**inconnu** pour construire le modèle ?
- Quelle **architecture** utiliser ?



## Support Jupyter pour la suite

[ici](jupyterNotebook/TD2/TDassessment.ipynb)
