---
author: PF Villard
title: Reconnaissance automatique de personnages basée sur les réseaux de neurones
---

## Plan de l'atelier
- Quelques bases théoriques
    - [L'**apprentissage automatique** par régression linéaire](html/IA1.html) (*10mn*)
    - [Les **réseaux de neurones**](html/IA2.html) (*10mn*)
    - [Les réseaux de neurones **profonds**](html/IA3.html) (*10mn*)
- [Application](asterix.html)  (*1h*)
    - **Acquisition** de données
    - **Apprentissage**
    - mise en production
- Problèmes d'[**éthique**](ethique.html) (*s'il reste du temps*)


## Cours

<a href="www.qr-code-generator.com/" border="0" style="cursor:default" rel="nofollow"><img src="https://chart.googleapis.com/chart?cht=qr&chl=https%3A%2F%2Fseshat.gitlabpages.inria.fr%2Fatelier-nsi%2F&chs=600x500&choe=UTF-8&chld=L|2"></a>


## Présentation de l'enseignant

Une présentation de l'enseignant se trouve [ici](pf.html)

## Références

- Images :
    -  [Wikipedia](https://en.wikipedia.org)
    -  [Machine Learning for Artists](https://ml4a.github.io/)
- D'après :
    - Le cours d'[Andrew Ng](https://en.wikipedia.org/wiki/Andrew_Ng) sur [Coursera](https://www.coursera.org/learn/machine-learning)
    - [Machine Learning for Artists](https://ml4a.github.io/)
    - [Deep Learning avec TensorFlow](https://ulysse.univ-lorraine.fr/permalink/33UDL_INST/1u932an/alma991010388389705596), Aurélien Géron, Dunod - 2017
    - l'article de Jason Brownlee sur [Deep Learning Performance](https://machinelearningmastery.com/learning-curves-for-diagnosing-machine-learning-model-performance/)


## Format des supports

- Supports en **HTML5**, **CSS3**
- Rédigé en [Markdown](https://daringfireball.net/projects/markdown/) et compilé par [pandoc](https://pandoc.org)
- fonctionnalités supplémentaires :
    - [reveal.js](https://github.com/hakimel/reveal.js/) **&rarr;** présentation dynamique
    - [chart.js](https://www.chartjs.org) **&rarr;** courbes
    - [MathJax](https://www.mathjax.org) **&rarr;** équations mathématiques
<!-- pandoc -t revealjs -s -o index.html intro.md -V revealjs-url=./reveal.js  -V transition=cube  -f markdown+smart -V theme=beige --mathjax --css slide.css -->

## Introduction

- Intelligence artificielle, apprentissages automatique et profond

![](https://ecoinfo.cnrs.fr/wp-content/uploads/2019/10/ia_ml_dl.png)
<!-- https://ecoinfo.cnrs.fr/2019/10/01/impact-environnemental-de-lia/ -->


## Outil utilisé dans l'atelier

- Programmation en **python** avec la bibliothèque **TensorFlow**
- Utilisation de la plateforme **Google Colab**
     - Interaction avec base de données via **Google Drive**
     - Utilisation de **GPU**

<div style="height:300px">![](https://i.morioh.com/b4d8a3dfdf.png)</div>



## Application concrète

- A partir de modèles [imprimés en 3D](https://gitlab.inria.fr/Seshat/deepAsterix/)
- **Acquisition** d'une base de donnée
- Conception de l'**architecture** 
- **Apprentissage**
- Mise en production dans une [appli web](https://members.loria.fr/PFVillard/files/asterix/essai.html)

![](https://gitlab.inria.fr/Seshat/deepAsterix/-/raw/master/paint/im12.jpg){height=300px}

## Suite

[l'apprentissage automatique](html/IA1.html)