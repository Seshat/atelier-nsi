---
author: PF Villard
title: Application à la Reconnaissance automatique de personnages
progress: true
slideNumber: true
navigationMode: 'linear'
---

# Préparation du matériel



## Impression 3D

<table>
<tr>
<td>

![](asterix/3D.jpg){height=300px}
</td>
<td>
<video height="300"  muted autoplay loop controls>
<source src="asterix/3Dprint2.mp4" type="video/mp4" />
</video>
</td>
</tr>
</table>

- 20 figurines d'Asterix ou Obelix
- Les fichiers se trouvent [ici](asterix/Asterix.stl) et [ici](asterix/Obelix.stl).



## Peinture

| ![](asterix/im0.jpg) | ![](asterix/im2.jpg)  | ![](asterix/im8.jpg)  | ![](asterix/im12.jpg)  |
| :------------------: | :-------------------: | :-----------------: | :-------: |



## Modèle 3D texturé

Utilisation de la méthode de dépliage UV :

![](asterix/uv.png){height=300px}



## Résultat

<iframe class="stretch"  data-src="https://members.loria.fr/PFVillard/files/asterix/asterix.html">
</iframe>
Résultat [ici](https://members.loria.fr/PFVillard/files/asterix/asterix.html)

# Développement (partie pratique)



## Base d'apprentissage

1. Faire une vidéo de chaque figurine de plusieurs minutes
2. Convertir la video en images, par exemple avec (ffmpeg)[https://ffmpeg.org] :
```
ffmpeg -i obelix.MOV -r 10    -vf scale=150:150 obelix/obelix%03d.jpg
```
avec :
   - ``obelix.MOV`` : le fichier video
   - ``10`` : la fréquence pour récupérer une image
   - ``150:150``la taille de l'image
   - ``obelix/obelix%03d.jpg`` la forme du fichier image en sortie
3. Faire un dossier pour **Obelix** et un dossier pour **Asterix**



## Exemple de video


<video loop autoplay controls muted>
<source src="asterix/asterixShort.mp4" type="video/mp4">
</video>



## Google colab

- Mettre les photos extraites dans votre [Google drive](https://drive.google.com/drive/my-drive)
- Il doit y avoir les dossiers `Asterix`et `Obelix`dans un dossier `Image`
- Dans [Google colab](https://colab.research.google.com/drive), ouvrir le squelette [deepAsterix.ipynb](deepAsterix.ipynb) 
- Suivre les indications

## Environnement

![](fig/im15.png)

## Environnement

![](fig/im16.png)

## Environnement

![](fig/im17.png)

## Apprentissage

- Plusieurs étapes :
    1. **Importer** les bibliothèques python
    2. **Charger les photos** des figurines avec `image_dataset_from_directory`
    3. Faire de l'**augmentation de donnée** avec `tensorflow.keras.layers`
    4. Définir un **modèle** d'architecture
    5. Effectuer l'apprentissage (~5mn)
    6. Tracer la **courbe d'apprentissage**
- **Analyser** la courbe d'apprentissage pour changer
    - **&rarr;** Modifier l'**augmentation de donnée** 
    - **&rarr;** Modifier le **taux d'apprentissage** (`learning_rate`)
    - **&rarr;** Modifier le **nombre d'epochs** (`epochs`)   

![](fig/obel.png){height=100px}


## Mise en production (1)<!-- omit in toc -->


- **Exporter** le modèle ([documentation](https://www.tensorflow.org/js/tutorials/conversion/import_keras))
- **Importer** le modèle :
```
const model = tf.loadLayersModel('model.json');
```
- **Lire l'image** à partir d'une page web, d'un dossier ou de la webcam
```
let image = document.getElementById('monImage');
```
![](fig/aste.png){height=200px}


## Mise en production (2)<!-- omit in toc -->
- **Ouvrir** le modèle
```
model.then(function (res) {
```    
- **Convertir** l'image dans le format TensorFlow  
```
    let example = tf.browser.fromPixels(image).expandDims();
```
- Changer la **taille**
```
    example = tf.image.resizeBilinear(example, [150, 150]).div(tf.scalar(255))
```    
- **Caster** en float
```
    example = tf.cast(example, dtype = 'float32');
```    
- **Prédire** la classe
```
    let prediction = res.predict(example);
    figurineClass=Math.round (prediction.dataSync());
```


## Mise en production (3)<!-- omit in toc -->
- Ajouter le résultat au HTML
```
    let tag = document.createElement("p");
    let text = document.createTextNode("Prediction : ");
    tag.appendChild(text);
    let text2 = document.createTextNode(classes[figurineClass]);
    tag.appendChild(text2);
    let element = document.getElementById(tags[i]);
    element.appendChild(tag);
}, function (err) {
    console.log(err);
});
```

[Exemple 1](https://members.loria.fr/PFVillard/files/asterix/essai.html) et [Exemple 2](https://members.loria.fr/PFVillard/files/asterix/)



---

## Exemple de résultat

| Image à tester | Résultat |
| :------------------: | :-------------------: | 
| ![](asterix/asterix082.jpg){height=50px}| Asterix |
| ![](asterix/obelix008.jpg){height=50px}| Obelix |
| ![](asterix/asterix140.jpg){height=50px}| Asterix |
| ![](asterix/obelix109.jpg){height=50px}| Obelix |

---

## Application

<img src="https://chart.googleapis.com/chart?cht=qr&chl=https%3A%2F%2Fasterix.micard-family.fr&chs=400x400&choe=UTF-8&chld=L|2" rel="nofollow" alt="qr code"><a href="www.qr-code-generator.com/" border="0" style="cursor:default" rel="nofollow"></a>

Lien : [ici](https://asterix.micard-family.fr)
