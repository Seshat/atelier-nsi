---
author: PF Villard
title: Apprentissage automatique
subtitle: par régression linéaire
progress: true
slideNumber: true
navigationMode: 'linear'
---

## Principe Général<!-- omit in toc -->

- Créer un modèle prédictif
- Régler les paramètres (avec données d’entraînement)
- Utiliser ce modèle pour faire des prédictions sur de nouvelles observations

<br/><br/>
<div class="container"  style="width: 100%;margin: 0 auto;">
<div class="col"  style="width: 100%;margin: 0 auto;">
 <img src=../fig/img1.png>
</div>
<div class="col"  style="width: 100%;margin: 0 auto;font-size:15px"> 
- Modèle : **y=ax+b**
- Paramètres : **a** et **b**
- Observations : ($x_1,y_1$),  ($x_2,y_2$), ... 
</div>
</div>

## Régression linéaire<!-- omit in toc -->

= somme pondérée des variables d’entrée + un terme constant
$$
\hat{y}=\theta_0+\theta_1 x_1 + \theta_2 x_2+...+\theta_n x_n
$$
avec :

- $\hat{y}$ la valeur prédite
- $n$ le nombre de variables
- $x_i$ la valeur de la $n^{ème}$ variable
- $\theta_j$ le $j^{ème}$ paramètre du modèle

## Système<!-- omit in toc --> 

On réalise plusieurs observations $x^{(1)}$, $x^{(2)}$,..., $x^{(m)}$

Cela permet de résoudre le système suivant :

$$
\begin{cases}
\hat{y}^{(1)}=\theta_0+\theta_1 x_1^{(1)} + \theta_2 x_2^{(1)}+...+\theta_n x_n^{(1)}\\
\hat{y}^{(2)}=\theta_0+\theta_1 x_1^{(2)} + \theta_2 x_2^{(2)}+...+\theta_n x_n^{(1)}\\
...\\
\hat{y}^{(m)}=\theta_0+\theta_1 x_1^{(m)} + \theta_2 x_2^{(m)}+...+\theta_n x_n^{(m)}\\
\end{cases}
$$


## Notation vectorielle <!-- omit in toc --> 

$$
\begin{cases}
\hat{y}^{(1)}=\theta_0+\theta_1 x_1^{(1)} + \theta_2 x_2^{(1)}+...+\theta_n x_n^{(1)}\\
\hat{y}^{(2)}=\theta_0+\theta_1 x_1^{(2)} + \theta_2 x_2^{(2)}+...+\theta_n x_n^{(2)}\\
...\\
\hat{y}^{(m)}=\theta_0+\theta_1 x_1^{(m)} + \theta_2 x_2^{(m)}+...+\theta_n x_n^{(m)}\\
\end{cases}
$$

devient sous forme vectorielle :
$$
\begin{gather}
\begin{bmatrix} 
\hat{y}^{(1)}\\
\hat{y}^{(2)}\\
...\\
\hat{y}^{(m)}
\end{bmatrix}=
\begin{bmatrix} 
1 &   x_1^{(1)} &  x_2^{(1)} &  ... &  x_n^{(1)}\\
1 &   x_1^{(2)}  & x_2^{(2)} &  ...  & x_n^{(2)}\\
...\\
1 &   x_1^{(m)}  & x_2^{(m)}  & ...  & x_n^{(m)}
\end{bmatrix}
\begin{bmatrix} 
\theta_0  \\
\theta_1 \\
\theta_2 \\
...\\
\theta_n
\end{bmatrix}
\end{gather}
$$ 

Soit : $\boldsymbol{\hat{\boldsymbol{y}}}= \boldsymbol{X} \boldsymbol{\theta}$


## Mesure de la précision du modèle<!-- omit in toc -->

- = mesure de l'erreur commise
- => Fonction de coût
- Le plus courant : RMSE

**racine carrée de l’erreur quadratique moyenne**

$$
RMSE(X,h)=\sqrt{\frac{1}{m}\sum_{i=1}^m \left[ h(x^{(i)})-y^{(i))} \right]^2}
$$

&rarr; Trouver les paramètres $\theta_j$  qui minimisent RMSE
&rarr; En pratique : **MSE**

![](https://seshat.gitlabpages.inria.fr/deeplearning/img/rmse.png){height=200px}


## Descente de gradient

- Algorithme d’optimisation très général
- Idée : corriger petit à petit les paramètres pour minimiser une fonction de coût
- Algo :
	1. remplir $\theta$ avec valeurs aléatoires
	2. améliorer progressivement pour que RMSE &searr;
	3. jusqu'à convergence vers minimum

![](https://miro.medium.com/max/1005/1*_6TVU8yGpXNYDkkpOfnJ6Q.png){height=300px}

## Problème <!-- omit in toc --> 

<div class="container">
<div class="col">
![](https://www.charlesbordet.com/assets/images/gradient-descent/gradientdescent-alpha0.05.gif)
</div>
<div class="col">
![](https://www.charlesbordet.com/assets/images/gradient-descent/gradientdescent-f3.gif)
</div>
</div>




## Exemple d'utilisation pratique

![](https://miro.medium.com/max/1400/1*70f9PB-RwFaakqD6lfp4iw.png)



## Descente de gradient stochastique
- Idée: ne pas utiliser tout le temps l'ensemble du jeu de données
- N'utiliser qu'une observation à chaque itération
    - &rarr; Moins régulier 
    - &rarr; Arrive au minimum sans converger
    - &rarr; Résultats bons mais pas optimums
    - &rarr; Peut permettre d'échapper aux minimums locaux
    - &rarr; Possibilité de réduire le taux d'apprentissage
  
![](https://miro.medium.com/max/1400/1*70f9PB-RwFaakqD6lfp4iw.png){height=150px}

## Descente de gradient par mini-lots<!-- omit in toc -->
- = **mini-batch gradient descend**
- A mi-chemin entre le DG ordinaire et le DG stochastique
- &rarr; calcul sur de petits sous-ensembles d'observation
- &rarr; Rapide avec le GPU !
- Peut avoir du mal à sortir d'un minimum local

![](https://miro.medium.com/max/1400/1*70f9PB-RwFaakqD6lfp4iw.png){height=150px}


## Courbes d’apprentissage

- Courbe pour modéliser les performances  
- &rarr; Permet de détecter des problèmes lors de l'apprentissage
  - Sous-apprentissage
  - Sur-apprentissage
  - Problèmes avec le volume des données
  
![](../fig/img6.png)

![](../fig/img7.png)



## En pratique<!-- omit in toc --> 

- Minimisation de l'erreur
- A chaque étape de calcul, l'algo d'entrainement doit être évalué
- Il y a 2 types de courbes :
      - Courbe d'apprentissage sur l'entrainement
      - Courbe d'apprentissage sur la validation



## Sous apprentissage<!-- omit in toc --> 

- Ne peut pas apprendre avec les données d'entraînement
  - Ligne plate 
  - Données bruitées avec valeurs hautes
  - Décroit jusqu'à la fin de l'entraînement
  
![](../fig/img8.png)   ![](../fig/img9.png)

## Sur-apprentissage<!-- omit in toc --> 

- Apprend trop y compris le bruit
- &rarr; **+** un modèle est précis **-** il est générique
  - Ligne continue qui décroit
  - Courbe de validation avec erreur &nearr;

![](../fig/img10.png)



## Bon apprentissage<!-- omit in toc --> 

- Stable, bon compromis
- Ecart minimal entre les courbes
- Appelé "*generalization gap*"

![](../fig/img11.png)

## Diagnostiquer des données pas représentatives<!-- omit in toc --> 

- Pas assez de **données d'entrainement**
    - pas assez de données pour apprendre un modèle générique
    - Amélioration sur l'entraînement
    - gap trop petit
- Pas assez de **données de validation**
    - Validation avec bruit
    - courbe en dessous
        - &rarr; plus facile à prédire

![](../fig/img12.png)  ![](../fig/img13.png)  ![](../fig/img14.png)


## Bilan de l'apprentissage automatique

- Il y a des **hyper-paramètres** à régler
    - Le **pas d'apprentissage** (descente de gradient)
    - La **méthode d'optimisation**
    - Il faut choisir une **fonction de coût**
- Le choix des **données d'entraînement** est important
- Le choix du **modèle** est important
- Le choix des **données de validation** est important
- L'analyse de la **courbe d'apprentissage** peut aider



## Suite

[Les **réseaux de neurones**](IA2.html)