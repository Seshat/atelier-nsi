---
author: PF Villard
title: Problèmes éthiques
progress: true
slideNumber: true
navigationMode: 'linear'
---

## Problématique de la validation

- Si tout est codé *normalement*, un ToolKit donne **toujours** un résultat
    -  **&rarr;** Il ne suffit pas d'avoir un code qui **compile** et qui s'**exécute**, il faut toujours **analyser** le résultat !
 -  **&rarr;** Savoir détecter des problèmes
 -  **&rarr;** Savoir tester l'efficacité d'un entraînement
 -  Eviter les problèmes comme dans cet [Exemple](https://www.sciencesetavenir.fr/high-tech/intelligence-artificielle/intelligence-artificielle-la-reconnaissance-faciale-est-elle-misogyne-et-raciste_121801) (autre [article](http://proceedings.mlr.press/v81/buolamwini18a/buolamwini18a.pdf) en anglais)

<div style="height:300px">![](https://www.sciencesetavenir.fr/assets/img/2018/03/06/cover-r4x3w1000-5a9e76d1e35f5-facerecog-iphone.jpg)</div>


## Impact sur le travail

- **Automatisation des tâches répétitives** et routinières. 
    - **Ex :** production industrielle, services administratifs.
- **Traitement des données** analyse massive des données rapide et précis.
    - **Ex :** finance, comptabilité 
- **Services clientèle** 
    - **Ex :** Les chatbots et agents conversationnels dans les centres d'appels et les services clientèle,
- **Transport et logistique** avec les véhicules autonomes
    - **Ex :** chauffeurs de taxi, chauffeurs de camion, livreurs
- **Métiers créatifs** : Génération de musique, d'images et autres formes d'art
    - **Ex :** musique, photographie et conception graphique.
- **Professions médicales** : diagnostiques, prescription de traitements
- **Services financiers** : analyses financiers et les gestionnaires de portefeuille
- Etc

## Impact sur la paresse intellectuelle

- **Sur-reliance sur l'IA** Humains peuvent devenir trop dépendants de l'IA pour effectuer des tâches simples
	- &rarr;  Compétences &searr; capacité de résolution de problèmes &searr;
- **Évitement des tâches difficiles** 
	- &rarr;   Capacité à relever des défis et à résoudre des problèmes complexes &searr;
- **Manque d'engagement intellectuel**
	- &rarr;    Motivation et créativité &searr;
- **Réduction de la mémoire à court terme** 
	- &rarr;   Compter sur l'IA pour stocker et récupérer des informations
- **Décisions rapides sans réflexion** :
 Erreurs de jugement et décisions impulsives.
 - **Réduction de l'empathie et de l'intuition**

## Impact sur l'environnement

- **Consommation élevée d'énergie** &larr; quantités de calculs, stockage de données
- **Production de déchets électroniques** &larr; remplacement fréquent de l'équipement avec substances toxiques
- **Extraction de matériaux** &larr;  production de l'équipement informatique
- **Impact sur les ressources en eau** &larr; refroidissement des équipements
- **Impacts sur la biodiversité** &larr; fragmentation et destruction des habitats




## Biais dans les apprentissages

- **Biais des données d'entraînement**
<!-- Si un modèle d'IA est entraîné sur des données historiques qui sont biaisées ou incomplètes, il peut reproduire ces biais dans ses prédictions.  -->
    - **Ex :** modèle d'embauche entraîné sur des données historiques qui ont mené à un manque de diversité dans l'entreprise
- **Biais de la sélection des caractéristiques** 
<!-- Si un modèle d'IA est entraîné sur un ensemble limité de caractéristiques ou de variables, il peut ne pas tenir compte d'informations importantes qui sont nécessaires pour faire des prédictions précises et équitables.  -->
    - **Ex :** modèle d'évaluation de crédit basé sur revenu et niveau d'éducation (pas l'historique de crédit ou les dépenses quotidiennes)
- **Biais de l'algorithme** 
<!-- Certains algorithmes d'IA peuvent avoir des biais intrinsèques, tels que la propension à privilégier certaines caractéristiques ou variables par rapport à d'autres. -->
    - **Ex :** algo de recommandation qui utilise la popularité pour classer les articles et pas ce qui est pertinent pour l'utilisateur
- **Biais de la conception** 
<!-- La conception d'un modèle d'IA peut introduire des biais, tels que l'utilisation de modèles mathématiques ou statistiques qui ne prennent pas en compte certaines variables ou caractéristiques importantes.  -->
    - **Ex :** prédiction de la récidive basée sur âge et historique criminel mais pas situation familiale ou accès aux soins de santé mentale
- **Biais de l'utilisateur** 
<!-- Les utilisateurs peuvent introduire des biais dans les résultats de l'IA en sélectionnant ou en interprétant les résultats d'une manière qui reflète leurs propres préjugés ou perspectives.  -->
    - **Ex :** tri de CV qui exclut candidats avec lacunes universitaire, même si circonstances extérieures (maladie, obligations familiales)
- **Biais culturels et sociaux**  
<!-- Les préjugés et les stéréotypes culturels et sociaux peuvent être involontairement intégrés dans les modèles d'IA, en raison de biais dans les données d'entraînement ou de la conception de l'algorithme. -->
    - **Ex :** prédiction de criminalité biaisé envers communautés minoritaires, si données basées sur arrestations disproportionnées dans ces communautés


## Quelques pistes

- **Établir des directives et des réglementations éthiques** 
	- &rarr; collaboration industrie / gouvernement  / université.
- **Éduquer le public** sur les impacts
- **Investir dans des programmes de reconversion**
	- &rarr; formation professionnelle  nouvelles compétences et se reconvertir
- Développer des applications d'IA qui **complètent** le travail humain plutôt que de le **remplacer**
	- &rarr; outils qui aident les travailleurs à effectuer des tâches plus efficacement ou plus sûrement.
- **Intégrer des considérations éthiques** dans conception et développement des systèmes d'IA 
