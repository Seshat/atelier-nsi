---
author: PF Villard
title: Les réseaux de neurones
progress: true
slideNumber: true
navigationMode: 'linear'
date: \today
---

## Neurones biologiques<!-- omit in toc -->

- Dans les cortex cérébraux, constitués de 
    - **corps cellulaire** (noyau+éléments)
    - Des prolongements appelés **dendrites**
    - Un très long prolongement appelé **axone**
          - décomposé en ramifications
          - se terminent par des **synapses**
          - **reliées** à d’autres neurones

![](https://upload.wikimedia.org/wikipedia/commons/thumb/1/10/Blausen_0657_MultipolarNeuron.png/640px-Blausen_0657_MultipolarNeuron.png){height=200px}
![](https://github.com/ml4a/ml4a.github.io/blob/master/images/neuron-simple.jpg?raw=true){height=200px}

<!--## Fonctionnement  omit in toc 

- courtes impulsions, appelées **signaux**
- si neurone reçoit (pdt qq mns) un nombre suffisant de signaux :
    - déclenche ses propres signaux
- organisés en un vaste réseau de **milliards** de neurones
  
![](https://upload.wikimedia.org/wikipedia/commons/5/5b/Cajal_cortex_drawings.png){height=200px}
-->


## neurone artificiel<!-- omit in toc -->

- premier neurone artificiel
    - plusieurs entrées **binaires** (**active**/**inactive**) 
    - une sortie **binaire**
- active sa sortie lorsque nombre entrées actives > un seuil
- calcule n’importe quelle proposition logique
- Exemple :

![](https://github.com/ml4a/ml4a.github.io/blob/master/images/neuron-artificial.png?raw=true){height=150px}

 - **&rarr;** Le neurone est activé si un ou plusieurs neurone $X_i$ sont activés


<!--## Perceptron omit in toc 

- inventé en 1957 par F.Rosenblatt
- architecture simple
- appelé unité linéaire à seuil (**LTU**, Linear Threshold Unit)
- Les entrées et la sortie sont à présent des nombres 

![](https://news.cornell.edu/sites/default/files/styles/story_thumbnail_xlarge/public/2019-09/0925_rosenblatt_main.jpg?itok=BCWmlVvO){height=200px}-->

## Perceptron<!-- omit in toc -->

- chaque connexion en entrée possède un poids
  - LTU calcule une somme pondérée des entrées
$$
 z = w_1 x_1 + w_2 x_2 + … + w_n x_n
$$
  - il applique une fonction d'**activation** (ex : step)
$$
hw(x) = step(z)
$$

![](https://miro.medium.com/max/2870/1*n6sJ4yZQzwKL9wnF5wnVNg.png){height=200px}




##

<iframe class="stretch"  data-src="https://ml4a.github.io/demos/forward_pass_mnist/">
</iframe>



  
<!-- ## Comment entraîne-t-on un perceptron ?omit in toc
- règle de Hebb : si un neurone biologique déclenche **souvent** un autre neurone, alors la connexion entre ces deux neurones **se renforce**
- perceptron reçoit une instance d’entraînement à la fois
-  effectue ses prédictions.
-  Pour chaque neurone de sortie qui produit une prédiction erronée, il **renforce les poids** des connexions **liées aux entrées** qui auraient contribué à la prédiction juste. -->




## Perceptron multicouche

- une **couche d’entrée** (&rarr; distribuer les entrées aux neurones de la couche suivante)
- une ou plusieurs couches de LTU appelées **couches cachées**
- une dernière couche de LTU appelée **couche de sortie**

![](https://upload.wikimedia.org/wikipedia/commons/a/a9/Perceptron_4layers.png){height=200px}

- Si + d'une couche cachée : **Deep Neural Network (DNN)**



## Rétropropagation<!-- omit in toc -->
- pour **chaque instance d’entraînement** :
    - effectuer une **prédiction** (passe vers l’avant)
    - **mesure l’erreur**, traverse chaque couche **en arrière** pour mesurer la **contribution à l’erreur** de chaque connexion (passe vers l’arrière)
    - se termine en ajustant légèrement les poids des connexions de manière à réduire l’erreur (étape de **descente de gradient**)


## hyperparamètres à ajuster<!-- omit in toc -->
- **topologie** de réseau (interconnexions des neurones)
- nombre de **couches**
- nombre de **neurones** par couche
- type de **fonction d’activation** (pour chaque couche)
- logique d’**initialisation** des poids
- etc.

## Fonction d’activation<!-- omit in toc -->

![](https://raw.githubusercontent.com/ml4a/ml4a.github.io/master/images/sigmoid.png){height=300px}
![](https://raw.githubusercontent.com/ml4a/ml4a.github.io/master/images/relu.png){height=300px}



## Suite

[Les réseaux de neurones **profonds**](IA3.html)
