#!/bin/sh

export myPandocSlide="pandoc -t revealjs -s  -V transition=cube   -f markdown -V theme="league" --mathjax    --css slide.css  -V revealjs-url=./reveal.js --template=myTemplate.html"


ls index.html
$myPandocSlide -o index.html markdown/intro.md 
$myPandocSlide -o asterix.html markdown/asterix.md 
$myPandocSlide -o ethique.html markdown/ethique.md 
ls index.html

$myPandocSlide -o selfAssessment1.html markdown/selfAssessment1.md
$myPandocSlide -o selfAssessment2.html markdown/selfAssessment2.md
$myPandocSlide -o pf.html markdown/pf.md

cp myTemplate.html html
cp -r common html
cp -r javascript html
cp -r reveal.js html
cp slide.css html



#for ((i = 1; i < 5; i++)); do
i=1
while [ "$i" -le 3 ]; do
    cat html/begin.html > otherStuff.txt  
    $myPandocSlide -o html/IA$i.html markdown/IA$i.md  --include-after-body=otherStuff.txt 
    $myPandocHTML -o docHtml/IA$i.html markdown/IA$i.md  --include-after-body=otherStuff.txt 
    $myPandocPDF -o pdf/IA$i.pdf markdown/IA$i.md 
    echo "IA$i done"
    i=$(( i + 1 ))
done
#rm *.txt